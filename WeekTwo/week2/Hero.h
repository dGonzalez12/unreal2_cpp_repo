#pragma once

//File hero.h
//Created 2021/05/18
//Copyright 2021 Diego

#ifndef __HEROHEADERGUARD__
#define __HEROHEADERGUARD__
#pragma once

class cHero {
public:
	void setName(std::string name) { 
		mName = name; 
	}

	void Greet();

protected:
	std::string name;
	int health;
	int power;
};


#endif
